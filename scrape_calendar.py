from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from ics import Calendar, Event, DisplayAlarm
import requests
import locale
import argparse
import pytz

def convert_to_utc(date, timezone="Europe/Amsterdam"):
    local_timezone = pytz.timezone(timezone)
    date = local_timezone.localize(date, is_dst=None)
    return date.astimezone(pytz.utc)

parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output-filename", required=True, help="The iCalendar file to save the dates into")
parser.add_argument("-p", "--postal-code", required=True, help="Your postal code")
parser.add_argument("-n", "--house-number", required=True, help="Your house number")
parser.add_argument("-l", "--locale", default="nl_NL")
parser.add_argument("-t", "--time-zone", default="Europe/Amsterdam")
args = parser.parse_args()


r = requests.get(f"https://www.rd4info.nl/NSI/Burger/Aspx/afvalkalender_public_text.aspx?pc={args.postal_code}&nr={args.house_number}&t=")

soup = BeautifulSoup(r.text, 'html.parser')

table_values = [table_value.text for table in soup.find_all("table", "plaintextMonth") for table_value in table.find_all("td")]
calendar_dict = dict(zip(table_values[::2], table_values[1::2]))

locale.setlocale(locale.LC_ALL, args.locale)

calendar = Calendar()
for date_string, event_type in calendar_dict.items():
    
    print(f"{date_string} - {event_type}") 
    date = datetime.strptime(date_string, '%A %d %B %Y')

    first_reminder_date = (date - timedelta(days=1)).replace(hour=9)
    first_alarm = DisplayAlarm(trigger=convert_to_utc(first_reminder_date, args.time_zone))

    second_reminder_date = date.replace(hour=7, minute=30)
    second_alarm = DisplayAlarm(trigger=convert_to_utc(second_reminder_date, args.time_zone))
    alarms = [first_alarm, second_alarm]

    if event_type == "PMD-afval":
        event_type = "PMD-afval (Plastic)"
    elif event_type == "GFT":
        event_type = "GFT (organic)"

    event = Event(name=event_type, begin=date, alarms=alarms)
    event.make_all_day()

    calendar.events.add(event)
 
with open(args.output_filename, "w") as calendar_file:
    calendar_file.writelines(calendar)
