# Afval Kalender

Parser voor de RD4 afval kalender. Om dit script te gebruiken eerst de benodigde packages installeren met:

```bash
$ pip install -r requirements.txt
```

Daarna script als volgt draaien:

```bash
$ python scrape_calendar -p {postcode} -n {huisnummer} -o {output-filename.ics}
```

Waarbij postcode uw postcode is, huisnummer uw huisnummer en output-filename.ics de naam van het ics calendar bestand dat
weggeschreven wordt. Dit ics bestand kan dan weer geimporteerd worden in een Calendar programma naar keuze (bijvoorbeeld op de iMac of iPhone).

